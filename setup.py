#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from setuptools import setup

PROJECT = 'ieee1394_config_rom'

with open("README.rst", "r") as fh:
    long_description = fh.read()

setup(
    name=PROJECT,
    version="0.2.0",
    description='IEEE Configuration ROM Parser',
    long_description=long_description,
    long_description_content_type="text/x-rst",
    author='Scott Bahling, Takashi Sakamoto (see README)',
    author_email='sbahling@mudgum.net',
    packages=[
        'ieee1394_config_rom',
        'ieee1394_config_rom.ieee1212',
    ],
    license='LGPL-3.0',
    classifiers=(
        "Programming Language :: Python :: 3",
        "Operating System :: POSIX :: Linux",
        "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
        "Development Status :: 4 - Beta",
        "Topic :: System :: Hardware :: Hardware Drivers",
        "Intended Audience :: Developers",
        "Environment :: Console",
    ),
)
