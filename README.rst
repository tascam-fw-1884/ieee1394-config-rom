==================================
IEEE 1394 Configuration ROM parser
==================================

Introduction
============

This Python module provides a base level parser for IEEE 1394 configuration
ROMs. The code was originally written by Takashi Sakamoto as part of the
hinawa-utils package [1]_. Since the original maintainer announced he would no
longer be maintaining the hinawa-utils code, I have broken it out into
a generic module.

The module include applications of below specifications:

 * IEEE 1212:2001 - IEEE Standard for a Control and Status Registers (CSR)
   Architecture for Microcomputer Buses
 * IEEE 1394:2008 - IEEE Standard for a High-Performance Serial Bus
 * AV/C Digital Interface Command Set General Specification Version 4.2
   (Sep. 2004, 1394 Trade Association)
 * AV/C Audio Subunit Specification 1.0 (Oct. 2000, 1394 Trade Association)
 * AV/C Connection and Compatibility Management Specification 1.1
   (Mar. 2003, 1394 Trade Association)
 * Configuration ROM for AV/C Devices 1.0 (Dec. 2000, 1394 Trade Association)

Requirements
============

 * Python 3.4 or later
    * https://docs.python.org/3/library/enum.html

License
=======

 * All modules are licensed under GNU Lesser General Public License version 3 or
   later.



.. [1] https://github.com/takaswie/hinawa-utils

